package com.heaboy.consumer.test.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.heaboy.service.test.entity.Test;
import com.heaboy.service.test.service.ITestService;
import com.heaboy.consumer.common.controller.BaseController;
import com.heaboy.common.common.web.AjaxResult;
import com.heaboy.service.generator.common.PageInfo;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;


/**
* <p>
* 前端控制器
* </p>
* @author heaboy
* @since 2019-03-11
*/
@Controller
@RequestMapping("/test/test")
public class TestController extends BaseController  {

    private String prefix = "test/test/";


    //ali提供的注解
    //dubbo提供的
    @Reference
    private ITestService testService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param test
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, Test test) {
        Page<Test> page = new Page<Test>(pageNo, pageSize);
        IPage<Test> pageInfo = testService.index(page, test);
        model.addAttribute("searchInfo", test);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param test
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Test test){
        return toAjax(testService.save(test));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("test",testService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param test
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Test test){
        return toAjax(testService.updateById(test));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(testService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(testService.removeByIds(ids));
    }

}

